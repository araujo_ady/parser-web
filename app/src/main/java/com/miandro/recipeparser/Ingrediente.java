package com.miandro.recipeparser;

/**
 * Created by miguel on 01/07/2017.
 */

public class Ingrediente {
    private String ingrediente;
    private int calorias, proteinas, HidratosDeCarbono, grasas, azucar, colesterol;
    private float sodio;
    Ingrediente(){

    }



    Ingrediente(String ingrediente, int calorias, int proteinas, int HidratosDeCarbono, int grasas, float sodio, int azucar, int colesterol) {
        this.ingrediente = ingrediente;
        this.calorias= calorias;
        this.proteinas=proteinas;
        this.HidratosDeCarbono= HidratosDeCarbono;
        this.grasas=grasas;
        this.sodio= sodio;
        this.azucar=azucar;
        this.colesterol=colesterol;

    }

    public String getIngrediente() {
        return ingrediente;
    }

    public void setIngrediente(String ingrediente) {
        this.ingrediente = ingrediente;
    }

    public int getCalorias() {
        return calorias;
    }

    public void setCalorias(int calorias) {
        this.calorias = calorias;
    }

    public int getProteinas() {
        return proteinas;
    }

    public void setProteinas(int proteinas) {
        this.proteinas = proteinas;
    }

    public int getHidratosDeCarbono() {
        return HidratosDeCarbono;
    }

    public void setHidratosDeCarbono(int hidratosDeCarbono) {
        HidratosDeCarbono = hidratosDeCarbono;
    }

    public int getGrasas() {
        return grasas;
    }

    public void setGrasas(int grasas) {
        this.grasas = grasas;
    }

    public float getSodio() {
        return sodio;
    }

    public void setSodio(float sodio) {
        this.sodio = sodio;
    }

    public int getAzucar() {
        return azucar;
    }

    public void setAzucar(int azucar) {
        this.azucar = azucar;
    }

    public int getColesterol() {
        return colesterol;
    }

    public void setColesterol(int colesterol) {
        this.colesterol = colesterol;
    }

    @Override
    public String toString() {
        return "Ingrediente{" +
                "Ingrediente='" + ingrediente + '\'' +
                ", calorias=" + calorias +
                ", proteinas=" + proteinas +
                ", HidratosDeCarbono=" + HidratosDeCarbono +
                ", grasas=" + grasas +
                ", sodio=" + sodio +
                ", azucar=" + azucar +
                ", colesterol=" + colesterol +
                '}';
    }
}
