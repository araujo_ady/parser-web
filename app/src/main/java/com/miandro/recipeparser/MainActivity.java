package com.miandro.recipeparser;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    final static String TAG = "RECIPEPARSER - OUT";
    DatabaseReference init;
    int ultimo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        final TextView text = (TextView) findViewById(R.id.texto);

        init = FirebaseDatabase.getInstance().getReference().child("Ingredientes").child("Init");
        init.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ultimo = Integer.parseInt(dataSnapshot.getValue().toString());
                Log.w(TAG, "El valor inicial es " + ultimo);

                ArrayList<String> attr = new ArrayList<String>();
                attr.add("Calories");
                attr.add("Protein");
                attr.add("Total Carbs");
                attr.add("Total Fat");
                attr.add("Sodium");
                attr.add("Sugars");
                attr.add("Cholesterol");

                for(int i = ultimo; i < Integer.MAX_VALUE; i++){
                    Log.w(TAG, "Vuelta " + i);
                    text.setText("Parseando id: " + i);
                    try {
                        Document doc = Jsoup.connect("http://www.myfitnesspal.es/en/food/calories/" + i).get();
                        ArrayList<String> valores = new ArrayList<String>();
                        String titulo = doc.select("title").toString();
                        if (titulo.contains("Genérico")) {
                            Log.w("TAG", i + " es generico.");
                            titulo = titulo.replace("<title>Calories in Genérico", "");
                            titulo = titulo.replace(" - Calories and Nutrition Facts | MyFitnessPal.com</title>", "");
                            valores.add(titulo);
                            Element table = doc.select("table").get(0); //select the first table.
                            Elements rows = table.select("tbody").select("tr");
                            for (int z = 0; z < rows.size(); z++) {
                                boolean attrNecesario = false;
                                Elements nums = rows.get(z).select("td");
                                for (int j = 0; j < nums.size(); j++) {

                                    if (nums.get(j).toString().contains("col-1")) {
                                        String nombre = nums.get(j).toString();
                                        nombre = nombre.replace("<td class=\"col-1\">", "");
                                        nombre = nombre.replace("</td>", "");
                                        if (attr.contains(nombre)) {
                                            //text.setText(text.getText() + " " + nombre);
                                            attrNecesario = true;
                                        }
                                    } else if (nums.get(j).toString().contains("col-2") && attrNecesario) {
                                        String atributo = nums.get(j).toString();
                                        atributo = atributo.replace("<td class=\"col-2\">", "");
                                        atributo = atributo.replace("</td>", "");
                                        atributo = atributo.replace(" mg", "");
                                        atributo = atributo.replace(" g", "");
                                        atributo = atributo.replace(",", ".");
                                        //text.setText(text.getText() + "\n" + atributo);
                                        valores.add(atributo);
                                        attrNecesario = false;
                                    }
                                }
                            }


                            //text.setText(doc.select("table").toString());

                            DatabaseReference firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Ingredientes");
                            firebaseDatabase.push().setValue(new Ingrediente(
                                    valores.get(0), //String ingrediente
                                    Integer.parseInt(valores.get(1)),//int calorias
                                    Integer.parseInt(valores.get(6)),//int proteinas
                                    Integer.parseInt(valores.get(4)),//int HidratosDeCarbono
                                    Integer.parseInt(valores.get(3)),//int grasas
                                    Float.parseFloat(valores.get(2)),//int sodio
                                    Integer.parseInt(valores.get(5)),//int azucar
                                    Integer.parseInt(valores.get(7))//int colesterol

                            ));
                        }
                        else {
                            Log.w(TAG, i + " no es generico");
                        }

                    } catch (Exception e) {
                        Log.w("ERROR en el elemento " + ultimo, e.toString());
                    }
                    DatabaseReference firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Ingredientes").child("Init");
                    firebaseDatabase.setValue(i);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        });
    }
}

